﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MathPractice : MonoBehaviour
{
    public Transform lightSource;

    public GameObject ObjectWithMesh;

    public Transform viewPoint;

    public int NumberOfPointsForPolygon = 3;
    public int pointsToSkip = 1;
    const float TAU = 6.283185307179586f;

    [SerializeField]
    private Camera m_Camera;
    [SerializeField]
    private Transform[] m_Points;
    [SerializeField]
    private float[] m_PointRadius;

    private void OnDrawGizmos()
    {
        //Mirror();
        CalculateSurfaceArea();
        //Turret();
        //DrawPolygon();
        //AdaptiveFieldOfView();
    }

    private void AdaptiveFieldOfView()
    {
        Vector3 cameraDir = m_Camera.transform.forward;

        for (int i = 0; i < m_Points.Length; i++)
        {
            UnityEditor.Handles.DrawWireArc(m_Points[i].position, m_Camera.transform.right, m_Points[i].position + new Vector3(m_PointRadius[i], 0.0f, 0.0f), 360.0f, m_PointRadius[i]);
        }

        Vector3 outerMostPoint = Vector2.zero;
        float lowestDot = float.MaxValue;
        for (int i = 0; i < m_Points.Length; i++)
        {
            Vector3 objectPosition = m_Points[i].position;
            Vector2 ObjDir = (objectPosition - m_Camera.transform.position).normalized;

            float dot = Vector2.Dot(cameraDir, ObjDir);
            
            if (dot < lowestDot)
            {
                lowestDot = dot;
                outerMostPoint = objectPosition;
            }
        }

        Gizmos.DrawLine(m_Camera.transform.position, outerMostPoint);
        //m_Camera.fieldOfView = 2.0f * Mathf.Atan2(opposite, adjacent) * Mathf.Rad2Deg;
    }

    void DrawPolygon()
    {
        Vector2[] vertices = new Vector2[NumberOfPointsForPolygon];
        for (int i = 0; i < NumberOfPointsForPolygon; i++)
        {
            float t = (float)i / (float)NumberOfPointsForPolygon;
            float angleRad = t * TAU;

            float x = Mathf.Cos(angleRad);
            float y = Mathf.Sin(angleRad);

            vertices[i] = new Vector2(x, y);
        }

        for (int i = 0; i < vertices.Length; i++)
        {
            int from = i;
            int to = (i + pointsToSkip) % vertices.Length;

            Gizmos.color = Color.green;
            Gizmos.DrawLine(vertices[from], vertices[to]);

            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(vertices[from], 0.1f);
        }
    }

    void Turret()
    {
        Vector3 sourcePosition = viewPoint.position;
        RaycastHit raycastHit;
        bool hit = Physics.Raycast(sourcePosition, viewPoint.forward, out raycastHit);

        if (hit)
        {
            Vector3 hitpoint = raycastHit.point;
            Vector3 upAxis = raycastHit.normal;
            Vector3 sourceDir = (hitpoint - sourcePosition).normalized;

            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(sourcePosition, hitpoint);
            Gizmos.color = Color.green;
            Gizmos.DrawLine(hitpoint, hitpoint + upAxis);

            Vector3 rightAxis = Vector3.Cross(upAxis, sourceDir);
            Gizmos.color = Color.red;
            Gizmos.DrawLine(hitpoint, hitpoint + rightAxis);

            Vector3 forwardAxis = Vector3.Cross(rightAxis, upAxis);
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(hitpoint, hitpoint + forwardAxis);

            Vector3[] vertices =
            {
                new Vector3(-1,0,1),
                new Vector3(1,0,1),
                new Vector3(-1,0,-1),
                new Vector3(1,0,-1),
                new Vector3(-1,2,1),
                new Vector3(1,2,1),
                new Vector3(-1,2,-1),
                new Vector3(1,2,-1)
            };

            Quaternion rotation = Quaternion.LookRotation(forwardAxis, upAxis);
            Matrix4x4 turretSpace = Matrix4x4.TRS(hitpoint,rotation, Vector3.one);
            for (int i = 0; i < vertices.Length; i++)
            {
                Gizmos.color = Color.magenta;
                Gizmos.DrawSphere(turretSpace.MultiplyPoint3x4(vertices[i]), 0.1f);
            }
        }
    }

    void CalculateSurfaceArea()
    {
        Mesh mesh = ObjectWithMesh.GetComponent<MeshFilter>().mesh;

        float finalArea = 0.0f;

        for (int i = 0; i < mesh.triangles.Length; i+=3)
        {
            int firstVertexIndex = mesh.triangles[i];
            int secondVertexIndex = mesh.triangles[i + 1];
            int thirdVertexIndex = mesh.triangles[i + 2];
            Vector3 firstVector = mesh.vertices[thirdVertexIndex] - mesh.vertices[firstVertexIndex];
            Vector3 secondVector = mesh.vertices[secondVertexIndex] - mesh.vertices[firstVertexIndex];

            Vector3 crossProduct = Vector3.Cross(firstVector, secondVector);
            float area = crossProduct.magnitude / 2.0f;
            finalArea += area;
        }

        int finalVertice = 0;
        for (int i = 0; i < mesh.vertexCount; i++)
        {
            if(mesh.vertices[i].x < mesh.vertices[finalVertice].x && mesh.vertices[i].y < mesh.vertices[finalVertice].y)
            {
                finalVertice = i;
            }
        }
        Gizmos.DrawSphere(mesh.vertices[finalVertice],0.1f);
        Debug.Log("Area : " + finalArea);
    }

    void Mirror()
    {
        Vector3 sourcePosition = lightSource.position;

        RaycastHit raycastHit;
        bool hit = Physics.Raycast(sourcePosition, lightSource.forward, out raycastHit);

        if (hit)
        {
            Vector3 sourceDir = (raycastHit.point - sourcePosition).normalized;
            Gizmos.color = Color.green;
            Gizmos.DrawLine(sourcePosition, raycastHit.point);
            Gizmos.color = Color.red;
            Gizmos.DrawLine(raycastHit.point, raycastHit.point + raycastHit.normal);
            float dotValue = Vector3.Dot(raycastHit.normal, sourceDir);

            Vector3 reflectedVectorDir = sourceDir - (2f * dotValue) * raycastHit.normal;
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(raycastHit.point, (raycastHit.point + reflectedVectorDir));
        }
        else
        {
            Gizmos.color = Color.white;
            Gizmos.DrawLine(sourcePosition, sourcePosition + lightSource.forward * 5f);
        }
    }
}
