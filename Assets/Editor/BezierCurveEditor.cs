﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BezierCurveCreator))]
public class BezierCurveEditor : Editor
{
    BezierCurveCreator m_BezierCurveCreator;
    PathData m_PathData;

    const float m_SegmentSelectDistanceThresold = 0.1f;
    int m_SelectedSegementIndex = -1;

    private void OnEnable()
    {
        m_BezierCurveCreator = target as BezierCurveCreator;
        m_PathData = m_BezierCurveCreator.PathData;
        SceneView.duringSceneGui += OnSceneViewGUI;
    }

    void OnDisable()
    {
        SceneView.duringSceneGui -= OnSceneViewGUI;
    }

    private void OnSceneViewGUI(SceneView sv)
    {
        Input();
        DrawScene();
    }

    void Input()
    {
        Event guiEvent = Event.current;
        Vector2 mousePos = HandleUtility.GUIPointToWorldRay(guiEvent.mousePosition).origin;

        if(guiEvent.type == EventType.MouseDown && guiEvent.button == 0 && guiEvent.shift)
        {
            if(m_SelectedSegementIndex != -1)
            {
                Undo.RecordObject(m_BezierCurveCreator, "Add Point In Between");
                m_PathData.AddPoint(mousePos, m_SelectedSegementIndex);
            }
            else if (!m_PathData.IsClosed)
            {
                Undo.RecordObject(m_BezierCurveCreator, "Add Point");
                m_PathData.AddPoint(mousePos);
            }
            
        }

        if (guiEvent.type == EventType.MouseDown && guiEvent.button == 1)
        {
            float minDistance = m_BezierCurveCreator.m_AnchorDiameter;
            int closestIndex = -1;
            
            for(int i = 0; i < m_PathData.CurveNodes.Count; i += 3)
            {
                float distance = Vector2.Distance(mousePos, m_PathData.CurveNodes[i]);
                if (distance < minDistance)
                {
                    minDistance = distance;
                    closestIndex = i;
                }
            }

            if(closestIndex != -1)
            {
                Undo.RecordObject(m_BezierCurveCreator, "Delete Point");
                m_PathData.DeletePoint(closestIndex);
            }
        }

        if(guiEvent.type == EventType.MouseMove)
        {
            float minSegmentDistance = m_SegmentSelectDistanceThresold;
            int newSelectedSegmentIndex = -1;

            for (int i = 0; i < m_PathData.TotalSegments; i++)
            {
                Vector3[] points = m_PathData.GetPoints(i);
                float distance = HandleUtility.DistancePointBezier(mousePos, points[0], points[3], points[1], points[2]);
                if(distance < minSegmentDistance)
                {
                    newSelectedSegmentIndex = i;
                    minSegmentDistance = distance;
                }
            }

            if(newSelectedSegmentIndex != m_SelectedSegementIndex)
            {
                m_SelectedSegementIndex = newSelectedSegmentIndex;
                HandleUtility.Repaint();
            }
        }
    }

    void DrawScene()
    {
        Handles.color = m_BezierCurveCreator.m_TangentColor;
        for (int index = 0; index < m_PathData.TotalSegments; index++)
        {
            Vector3[] Points = m_PathData.GetPoints(index);
            if (m_BezierCurveCreator.m_DisplayControlPoints)
            {
                Handles.DrawLine(Points[0], Points[1]);
                Handles.DrawLine(Points[3], Points[2]);
            }
            Color segmentColor = (index == m_SelectedSegementIndex) ? m_BezierCurveCreator.m_SelectedSegmentColor : m_BezierCurveCreator.m_SegmentColor;
            Handles.DrawBezier(Points[0], Points[3], Points[1], Points[2], segmentColor, null, 2.0f);
        }

        
        for (int index = 0; index < m_PathData.CurveNodes.Count; index++)
        {
            if (index % 3 == 0 || m_BezierCurveCreator.m_DisplayControlPoints)
            {
                Color handleColor = (index % 3 == 0) ? m_BezierCurveCreator.m_AnchorColor : m_BezierCurveCreator.m_ControlColor;
                float handleSize = (index % 3 == 0) ? m_BezierCurveCreator.m_AnchorDiameter : m_BezierCurveCreator.m_ControlDiameter;
                Handles.color = handleColor;
                Vector3 newPos = Handles.FreeMoveHandle(m_PathData.CurveNodes[index], Quaternion.identity, handleSize, Vector3.zero, Handles.CylinderHandleCap);
                if (m_PathData.CurveNodes[index] != newPos)
                {
                    Undo.RecordObject(m_BezierCurveCreator, "Move Point");
                    m_PathData.MovePoint(index, newPos);
                }
            }
        }
    }

    public override void OnInspectorGUI()
    {
        DrawInspector();
    }

    void DrawInspector()
    {
        DrawDefaultInspector();

        EditorGUI.BeginChangeCheck();
        if (GUILayout.Button("Create New Curve"))
        {
            Undo.RecordObject(m_BezierCurveCreator, "Create Curve");
            m_PathData = m_BezierCurveCreator.CreateCurve();
        }
        if (GUILayout.Button("Add Point"))
        {
            Undo.RecordObject(m_BezierCurveCreator, "Add Point");
            m_PathData.AddPointAtBack();
        }
        if (GUILayout.Button("Reset"))
        {
            Undo.RecordObject(m_BezierCurveCreator, "Reset Curve");
            m_PathData.Reset();
            m_PathData = m_BezierCurveCreator.CreateCurve();
        }

        bool closePath = GUILayout.Toggle(m_PathData.IsClosed, "Close Path");
        if(closePath != m_PathData.IsClosed)
        {
            Undo.RecordObject(m_BezierCurveCreator, "Toggle Path");
            m_PathData.IsClosed = closePath;
        }

        bool autoSetValue = GUILayout.Toggle(m_PathData.AutoSetControlPoints, "Auto Set Path");
        if(autoSetValue != m_PathData.AutoSetControlPoints)
        {
            Undo.RecordObject(m_BezierCurveCreator, "Toggle Auto Set Path");
            m_PathData.AutoSetControlPoints = autoSetValue;
        }

        if (EditorGUI.EndChangeCheck())
        {
            SceneView.RepaintAll();
        }
    }
}
