﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BezierCurve : MonoBehaviour
{
    [SerializeField] private Transform m_PointA;
    [SerializeField] private Transform m_PointB;
    [SerializeField] private Transform m_PointC;
    [SerializeField] private Transform m_PointD;

    [SerializeField] private Transform m_PointAB;
    [SerializeField] private Transform m_PointBC;
    [SerializeField] private Transform m_PointCD;

    [SerializeField] private Transform m_PointAB_BC;
    [SerializeField] private Transform m_PointBC_CD;

    [SerializeField] private Transform m_PointABCD;

    float interpolatedAmount = 0.0f;

    private void Update()
    {
        interpolatedAmount = (interpolatedAmount + Time.deltaTime) % 1.0f;
       // m_PointAB.position = Vector3.Lerp(m_PointA.position, m_PointB.position, interpolatedAmount);
        //m_PointBC.position = Vector3.Lerp(m_PointB.position, m_PointC.position, interpolatedAmount);
        //m_PointCD.position = Vector3.Lerp(m_PointC.position, m_PointD.position, interpolatedAmount);

        //m_PointAB_BC.position = QuadraticBezierCurve(m_PointA.position, m_PointB.position, m_PointC.position, interpolatedAmount);
        //m_PointBC_CD.position = QuadraticBezierCurve(m_PointB.position, m_PointC.position, m_PointD.position, interpolatedAmount);

        m_PointABCD.position = CubicBezierCurve(m_PointA.position, m_PointB.position, m_PointC.position, m_PointD.position, interpolatedAmount);
    }

    Vector3 QuadraticBezierCurve(Vector3 a, Vector3 b, Vector3 c, float t)
    {
        Vector3 ab = Vector3.Lerp(a, b, t);
        Vector3 bc = Vector3.Lerp(b, c, t);

        return Vector3.Lerp(ab, bc, t);
    }

    Vector3 CubicBezierCurve(Vector3 a, Vector3 b, Vector3 c, Vector3 d, float t)
    {
        Vector3 ab_bc = QuadraticBezierCurve(a, b, c, t);
        Vector3 bc_cd = QuadraticBezierCurve(b, c, d, t);

        return Vector3.Lerp(ab_bc, bc_cd, t);
    }
}
