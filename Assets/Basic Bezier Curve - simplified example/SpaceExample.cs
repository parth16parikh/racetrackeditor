﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceExample : MonoBehaviour
{
    [SerializeField]
    private Transform m_LocalCordTransform;

    [SerializeField]
    private Vector2 m_LocalPosition;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;

        Vector2 finalPosition = LocalToWorld(m_LocalPosition);

        Gizmos.DrawSphere(finalPosition, 0.05f);

    }

    Vector2 LocalToWorld(Vector2 localPoint)
    {
        Vector2 worldDirection = transform.right * localPoint.x + transform.up * localPoint.y;
        Vector2 worldDirectionBtwOrigin = m_LocalCordTransform.position - transform.position;
        return worldDirection + worldDirectionBtwOrigin;
    }
}
