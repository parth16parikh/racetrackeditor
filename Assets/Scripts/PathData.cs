﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PathData
{
    [SerializeField]
    [HideInInspector]
    List<Vector3> m_CurveNodes;
    [SerializeField]
    [HideInInspector]
    bool m_IsClosed = false;
    [SerializeField]
    [HideInInspector]
    bool m_AutoSetControlPoints = false;

    public bool IsClosed
    {
        get
        {
            return m_IsClosed;
        }
        set
        {
            if(m_IsClosed != value)
            {
                m_IsClosed = value;
                if (m_IsClosed)
                {
                    m_CurveNodes.Add(m_CurveNodes[m_CurveNodes.Count - 1] + (m_CurveNodes[m_CurveNodes.Count - 1] - m_CurveNodes[m_CurveNodes.Count - 2]));
                    m_CurveNodes.Add(m_CurveNodes[0] + (m_CurveNodes[0] - m_CurveNodes[1]));
                    if (AutoSetControlPoints)
                    {
                        AutoSetAnchorPoints(0);
                        AutoSetAnchorPoints(m_CurveNodes.Count - 3);
                    }
                }
                else
                {
                    m_CurveNodes.RemoveRange(m_CurveNodes.Count - 2, 2);
                    if (AutoSetControlPoints)
                    {
                        AutoSetStartEndControlPoints();
                    }
                }
            }
        }
    }
    public bool AutoSetControlPoints
    {
        get
        {
            return m_AutoSetControlPoints;
        }
        set
        {
            if (m_AutoSetControlPoints == value)
            {
                return;
            }
            m_AutoSetControlPoints = value;
            if (m_AutoSetControlPoints)
            {
                AutoSetAllControlPoints();
            }
        }
    }

    public List<Vector3> CurveNodes
    {
        get
        {
            return m_CurveNodes;
        }
        private set
        {
            m_CurveNodes = value;
        }
    }

    public int TotalSegments
    {
        get
        {
            return (m_CurveNodes.Count / 3);
        }
    }

    public PathData()
    {
        m_CurveNodes = new List<Vector3>{
            Vector3.zero,
            Vector3.up,
            new Vector3(1, -1, 0),
            Vector3.right
        };
    }

    public void AddPointAtBack()
    {
        if (m_IsClosed)
        {
            Debug.LogError("You can not add point when path is closed");
            return;
        }
        Vector3 lastPoint = m_CurveNodes[m_CurveNodes.Count - 1];
        Vector3 newPoint = lastPoint + Vector3.right;
        AddPoint(newPoint);
    }

    public void DeletePoint(int index)
    {
        if (m_IsClosed && m_CurveNodes.Count < 3) return;
        if (m_CurveNodes.Count < 2) return;

        if(index == 0)
        {
            if (m_IsClosed)
            {
                m_CurveNodes[m_CurveNodes.Count - 1] = m_CurveNodes[2];
            }
            m_CurveNodes.RemoveRange(0, 3);
        }
        else if(index == m_CurveNodes.Count-1 && !m_IsClosed)
        {
            m_CurveNodes.RemoveRange(index - 2, 3);
        }
        else
        {
            m_CurveNodes.RemoveRange(index - 1, 3);
        }
    }

    public void AddPoint(Vector3 position, int segmentIndex)
    {
        m_CurveNodes.InsertRange(segmentIndex * 3 + 2, new Vector3[] { Vector3.zero, position, Vector3.zero });

        if (m_AutoSetControlPoints)
        {
            AutoSetAffectedControlPoints(segmentIndex * 3 + 3);
        }
        else
        {
            AutoSetAnchorPoints(segmentIndex * 3 + 3);
        }
    }

    public void AddPoint(Vector3 point)
    {
        if (m_IsClosed)
        {
            Debug.LogError("You can not add point when path is closed");
            return;
        }
        Vector3 lastPoint = m_CurveNodes[m_CurveNodes.Count - 1];
        Vector3 lastControlPoint = m_CurveNodes[m_CurveNodes.Count - 2];

        Vector3 direction = (lastPoint - lastControlPoint).normalized;
        float magnitude = (lastPoint - lastControlPoint).magnitude;

        m_CurveNodes.Add(lastPoint + (direction * magnitude));
        m_CurveNodes.Add(point + Vector3.up);
        m_CurveNodes.Add(point);

        if (AutoSetControlPoints)
        {
            AutoSetAffectedControlPoints(m_CurveNodes.Count - 1);
        }
    }

    public Vector3[] GetPoints(int index)
    {
        Vector3[] points = new Vector3[4];
        int startPoint = index * 3;
        points[0] = m_CurveNodes[startPoint];
        points[1] = m_CurveNodes[startPoint + 1];
        points[2] = m_CurveNodes[startPoint + 2];
        points[3] = m_CurveNodes[LoopIndex(startPoint + 3)];
        return points;
    }

    public Vector3[] CalculateEvenlySpacedPoints(float spacing, float resolution = 1.0f)
    {
        List<Vector3> evenlySpacedPoints = new List<Vector3>();
        evenlySpacedPoints.Add(m_CurveNodes[0]);
        Vector3 previousPoint = m_CurveNodes[0];
        float distanceFromLastPoint = 0.0f;

        for (int segmentIndex = 0; segmentIndex < TotalSegments; segmentIndex++)
        {
            Vector3[] points = GetPoints(segmentIndex);
            float t = 0;
            float controlNetLength = (Vector3.Distance(points[0], points[1]) + Vector3.Distance(points[1], points[2]) + Vector3.Distance(points[2], points[3]));
            float estimatedCurveLength = Vector3.Distance(points[0], points[3]) + controlNetLength/2.0f;
            float divisions = (estimatedCurveLength * resolution * 10.0f);
            while(t <= 1)
            {
                t += (1.0f/divisions);
                Vector3 pointOnCurve = Bezier.EvaluateCubic(points[0], points[1], points[2], points[3], t);
                distanceFromLastPoint += Vector3.Distance(previousPoint, pointOnCurve);

                while(distanceFromLastPoint >= spacing)
                {
                    float overShootDistance = distanceFromLastPoint - spacing;
                    Vector3 newEvenlySpacedPoint = pointOnCurve + (previousPoint - pointOnCurve).normalized * distanceFromLastPoint;
                    evenlySpacedPoints.Add(newEvenlySpacedPoint);
                    distanceFromLastPoint = overShootDistance;
                    previousPoint = newEvenlySpacedPoint;
                }

                previousPoint = pointOnCurve;
            }
        }

        return evenlySpacedPoints.ToArray();
    }

    void AutoSetAffectedControlPoints(int updatedIndex)
    {
        for (int i = updatedIndex - 3; i <= updatedIndex + 3; i += 3)
        {
            if (i >= 0 && i < m_CurveNodes.Count || m_IsClosed)
            {
                AutoSetAnchorPoints(LoopIndex(i));
            }
        }
        AutoSetStartEndControlPoints();
    }

    void AutoSetAllControlPoints()
    {
        for (int i = 0; i < m_CurveNodes.Count; i += 3)
        {
            AutoSetAnchorPoints(i);
        }
        AutoSetStartEndControlPoints();
    }

    void AutoSetAnchorPoints(int anchorIndex)
    {
        Vector3 anchorPos = m_CurveNodes[anchorIndex];
        Vector3 direction = Vector3.zero;
        float[] neighbourMagnitude = new float[2];

        if (anchorIndex - 3 >= 0 || m_IsClosed)
        {
            Vector3 offsetVector = m_CurveNodes[LoopIndex(anchorIndex - 3)] - anchorPos;
            direction += offsetVector.normalized;
            neighbourMagnitude[0] = offsetVector.magnitude;
        }

        if (anchorIndex + 3 < m_CurveNodes.Count || m_IsClosed)
        {
            Vector3 offsetVector = m_CurveNodes[LoopIndex(anchorIndex + 3)] - anchorPos;
            direction -= offsetVector.normalized;
            neighbourMagnitude[1] = -offsetVector.magnitude;
        }

        direction.Normalize();

        for (int i = 0; i < 2; i++)
        {
            int controlIndex = anchorIndex + (i * 2) - 1;
            if(controlIndex >= 0 && controlIndex < m_CurveNodes.Count || m_IsClosed)
            {
                m_CurveNodes[LoopIndex(controlIndex)] = anchorPos + direction * neighbourMagnitude[i] * 0.5f;
            }
        }
    }

    void AutoSetStartEndControlPoints()
    {
        if (!m_IsClosed)
        {
            m_CurveNodes[1] = (m_CurveNodes[0] + m_CurveNodes[2]) * 0.5f;
            m_CurveNodes[m_CurveNodes.Count - 2] = (m_CurveNodes[m_CurveNodes.Count - 1] + m_CurveNodes[m_CurveNodes.Count - 3]) * 0.5f;
        }
    }

    public void MovePoint(int index, Vector3 newPos)
    {
        Vector3 deltaMove = newPos - m_CurveNodes[index];

        if (!AutoSetControlPoints || index % 3 == 0)
        {
            m_CurveNodes[index] = newPos;

            if (AutoSetControlPoints)
            {
                AutoSetAffectedControlPoints(index);
            }
            else
            {
                if (index % 3 == 0)
                {
                    //it is a anchor point
                    if ((index + 1) < m_CurveNodes.Count || m_IsClosed)
                    {
                        m_CurveNodes[LoopIndex(index + 1)] += deltaMove;
                    }

                    if ((index - 1) >= 0 || m_IsClosed)
                    {
                        m_CurveNodes[LoopIndex(index - 1)] += deltaMove;
                    }
                }
                else
                {
                    //it is control point
                    //bool nextPointIsAnchorPoint = ((index + 1) % 3 == 0);
                    //int correspondingControlIndex = nextPointIsAnchorPoint ? index + 2 : index - 2;
                    //int anchorPointIndex = nextPointIsAnchorPoint ? index + 1 : index - 1;

                    //if (correspondingControlIndex >= 0 && correspondingControlIndex < m_CurveNodes.Count || m_IsClosed)
                    //{
                    //    Vector3 direction = (m_CurveNodes[LoopIndex(anchorPointIndex)] - m_CurveNodes[LoopIndex(index)]).normalized;
                    //    float magnitude = (m_CurveNodes[LoopIndex(correspondingControlIndex)] - m_CurveNodes[LoopIndex(anchorPointIndex)]).magnitude;
                    //    m_CurveNodes[LoopIndex(correspondingControlIndex)] = m_CurveNodes[LoopIndex(anchorPointIndex)] + (direction * magnitude);
                    //}
                }
            }
        }

    }

    int LoopIndex(int i)
    {
        return (i + m_CurveNodes.Count) % m_CurveNodes.Count;
    }

    public void Reset()
    {
        m_CurveNodes.Clear();
    }
}
