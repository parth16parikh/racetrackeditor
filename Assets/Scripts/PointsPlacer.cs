﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsPlacer : MonoBehaviour
{
    public float spacing = 0.1f;
    public float resolution = 1f;

    private void Start()
    {
        Vector3[] evenlySpacedPoints = GetComponent<BezierCurveCreator>().PathData.CalculateEvenlySpacedPoints(spacing, resolution);

        foreach (Vector3 item in evenlySpacedPoints)
        {
            GameObject go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            go.transform.position = item;
            go.transform.localScale = Vector3.one * spacing * 0.5f;
        }
    }

}
