﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BezierCurveCreator : MonoBehaviour
{
    [HideInInspector][SerializeField]
    private PathData m_PathData;

    public Color m_AnchorColor = Color.red;
    public Color m_ControlColor = Color.white;
    public Color m_SegmentColor = Color.green;
    public Color m_SelectedSegmentColor = Color.yellow;
    public Color m_TangentColor = Color.black;

    public float m_AnchorDiameter = 0.1f;
    public float m_ControlDiameter = 0.06f;
    public bool m_DisplayControlPoints = true;

    public PathData PathData
    {
        get
        {
            return m_PathData;
        }
        private set
        {
            m_PathData = value;
        }
    }

    public PathData CreateCurve()
    {
        m_PathData = new PathData();
        return m_PathData;
    }
}
